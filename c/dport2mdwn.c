#include "dport2mdwn.h"
typedef struct {
    pthread_t thread;
    pthread_attr_t thread_attr;
    char *path;
    int pipe;
    Category *cat;
} ThreadTarget;


void process_categories(char *, int);
void * process_package(void *);

char *input = "/home/harald/DPorts/";
char *output = "/home/harald/dport_to_wiki/output/";

int main(int argc, char** argv){
    int opt;
    int threads = 1;
    extern int optind;

    while((opt = getopt(argc, argv, "j")) != -1){
        switch(opt){
            case 'j':
                if(optind < argc) {
                    threads = (atoi(argv[optind]) < 1)?1:atoi(argv[optind]);
                }
                break;
            default:
                printf("valid options:\n-j <number of threads>\n");
                exit(1);
                break;
        }
    }

    process_categories(input, threads);

    return 0;

}




void
process_categories(char *path, int threads){
    DirList *dl = new_DirList();
    DirList *cdl;
    int pipes[2];
    char result;
    int running = 0;
    ThreadTarget *tt;

    pipe(pipes);

    get_directories(path, dl);
    for(int x=0; x < dl->cnt; x++){
        if(strcmp(".git",dl->directories[x]->name) != 0){
            /* process each category directory with a free thread */
            cdl = new_DirList();
            get_directories(dl->directories[x]->path, cdl);
            for(int y = 0; y < cdl->cnt; y++){
                if(running == threads && (1 == (read(pipes[0], &result, 1)))){
                        running--;
                }
            
                tt = calloc(sizeof(ThreadTarget), sizeof(char));
                tt->path = cdl->directories[y]->path;
                tt->pipe = pipes[1];
                pthread_create(&tt->thread, &tt->thread_attr, process_package, tt);
                running++;
            }
            free_DirList(cdl);
        }
    }
    /* make sure we wat until all threads are finished */
    while(running > 0) {
        if(1 == (read(pipes[0], &result, 1))){
            running--;
        }
    }
    close(pipes[0]);
    close(pipes[1]);
    free_DirList(dl);

}


void *
process_package(void * arg){
    ThreadTarget *tt = (ThreadTarget *) arg;
    Package *pkg = new_Package();
    char end = 1;
    char *make = combine_paths(tt->path, "Makefile");
    char *desc = combine_paths(tt->path, "pkg-descr");

    parse_makefile(make, pkg);
    parse_pkg_descr(desc, pkg);
    
    write(tt->pipe,&end ,1);
    free(make);
    free(desc);
    free_Package(pkg);
    free(tt);
    return NULL;
}

