#include "dport2mdwn.h"

char *
combine_paths(char *one, char *two){
    if(one == NULL || two == NULL) return NULL;
    int combined = strlen(one) + strlen(two) + 2;
    char *buf = (char *) calloc(combined, sizeof(char));
    int l = strlen(one);
    strncpy(buf, one, (l + 1));
    if(one[l - 1] != '/' && two[0] != '/'){
        strncat(buf, "/", 2);
    }
    strncat(buf, two, strlen(two) + 1);
    return buf;
}

/* return in DirList *result all directories that:
 * - start lowercase (categories nor package directories start uppercase)
 * - aren't . or ..
 */
int
get_directories(char *path, DirList *result){
    struct dirent *de = NULL;
    DIR *ds = opendir(path);
    int dirFnd = 0;
    struct stat info;

    while((de = readdir(ds))){
        dirFnd = 0;
        if(de->d_type == DT_DIR
                && is_lowercase(de->d_name[0])
                && strcmp(de->d_name, ".") != 0
                && strcmp(de->d_name, "..") != 0
                && strcmp(de->d_name, ".git") != 0
                ){
            dirFnd = 1;
        }
        else if (de->d_type == DT_UNKNOWN){
            if(lstat(combine_paths(path, de->d_name), &info) == 0
                    && S_ISDIR(info.st_mode)
                    && strcmp(de->d_name, ".") != 0
                    && strcmp(de->d_name, "..") != 0
                    && is_lowercase(de->d_name[0])
                    && strcmp(de->d_name, ".git") != 0
                    ){
                dirFnd = 1;
            }
        }

        if(dirFnd){
            push_to_dirlist(result,new_Directory_using(de->d_name, combine_paths(path, de->d_name)));
        }
    }

    if(ds){ closedir(ds); }
    return 1;
}

Directory *
new_Directory(){
    Directory *d = (Directory *) malloc(sizeof(Directory));
    if(d == NULL) return NULL;

    d->name = (char *) calloc(PATH_MAX, sizeof(char));
    d->path = (char *) calloc(PATH_MAX, sizeof(char));
    return d;
}

Directory *
new_Directory_using(char *name, char *path){
    Directory *d = new_Directory();
    if(d == NULL) return NULL;

    free(d->name);
    free(d->path);
    d->name = (char *) calloc(strlen(name) + 1, sizeof(char));
    d->path = (char *) calloc(strlen(path) + 1, sizeof(char));
    d->name = strncpy(d->name, name, strlen(name) + 1);
    d->path = strncpy(d->path, path, strlen(path) + 1);
    return d;
}
void
free_Directory(Directory *dir){
    if(dir == NULL) return;
    if(dir->name) free(dir->name);
    if(dir->path) free(dir->path);
    free(dir);
}

DirList *
new_DirList(){
    DirList *dl = (DirList *) malloc(sizeof(DirList));
    if(dl == NULL) return NULL;

    dl->directories = (Directory **) calloc(LST_BASE,sizeof(Directory*));
    dl->cap = LST_BASE;
    dl->cnt = 0;
    return dl;
}
void
free_DirList(DirList *dl){
    if(dl == NULL) return;

    for(int x = 0; x < dl->cnt; x++){
        free_Directory(dl->directories[x]);
    }
    free(dl);
}

Package *
new_Package(){
    Package *p = (Package *) calloc(sizeof(Package), sizeof(char));
    if(p == NULL) return NULL;

    p->urls = (char **) calloc(LST_BASE, sizeof(char *));
    p->urls_cap = LST_BASE;

    p->categories = (char **) calloc(LST_BASE, sizeof(char *));
    p->categories_cap = LST_BASE;

    p->uses = (char **) calloc(LST_BASE, sizeof(char *));
    p->uses_cap = LST_BASE;

    p->logs = (char **) calloc(LST_BASE, sizeof(char *));
    p->logs_cap = LST_BASE;

    return p;
}
void
free_Package(Package *pkg){
    if(pkg == NULL) return;
    if(pkg->urls != NULL){
        for(int x = 0; x < pkg->urls_cnt; x++){
            free(pkg->urls[x]);
        }
    }
    if(pkg->categories != NULL){
        for(int x = 0; x < pkg->categories_cnt; x++){
            free(pkg->categories[x]);
        }
    }
    if(pkg->uses != NULL){
        for(int x = 0; x < pkg->uses_cnt; x++){
            free(pkg->uses[x]);
        }
    }
    if(pkg->logs != NULL){
        for(int x = 0; x < pkg->logs_cnt; x++){
            free(pkg->logs[x]);
        }
    }

    free(pkg->name);
    free(pkg->portname);
    free(pkg->maintainer);
    free(pkg->hidden_maintainer);
    free(pkg->hashed_maintainer);
    free(pkg->version);
    free(pkg->portversion);
    free(pkg->distversion);
    free(pkg->previous_version);
    free(pkg->current_version);
    free(pkg->comment);
    free(pkg->description);
    free(pkg->users);
    free(pkg->groups);
    free(pkg->binfiles);
    free(pkg->broken);
    free(pkg->license);

    free(pkg->urls);
    free(pkg->logs);
    free(pkg->uses);
    free(pkg->categories);
    free(pkg);
}

Category *
new_Category(){
    Category *c = (Category *) calloc(sizeof(Category), sizeof(char));
    if(c == NULL) return NULL;

    c->packages = (Package **) calloc(PKG_BASE, sizeof(Package *));
    c->packages_cap = PKG_BASE;
    return c;
}
void
free_Category(Category *cat){
    for(int x = 0; x < cat->packages_cnt; x++){
        free_Package(cat->packages[x]);
    }
    free(cat->packages);
    free(cat->name);
    free(cat);
}

int
category_add_package(Category *cat, Package *pkg){
    if(pkg == NULL || cat == NULL) return 0;

    if(cat->packages_cnt == cat->packages_cap){
        Package **ph = (Package **) reallocarray(cat->packages, cat->packages_cap * 2, sizeof(Package *));
        if(ph){
            cat->packages = ph;
            cat->packages_cap *= 2;
        } else { return 0; }
    }
    cat->packages[cat->packages_cnt] = pkg;
    cat->packages_cnt++;

    return 1;
}
int
push_to_dirlist(DirList *dirlist, Directory *dir){
    if(dir == NULL || dirlist == NULL) return 0;

    /* dirlist holding array too small, resize */
    if((dirlist->cnt + 1) > dirlist->cap){
        Directory **dirs = (Directory**) reallocarray(dirlist->directories, dirlist->cap *  2 , sizeof(Directory*));
        if(dirs){
            dirlist->directories = dirs;
            dirlist->cap *= 2;
        } else {
            return 0;
        }

    }
    dirlist->directories[dirlist->cnt] = dir;
    dirlist->cnt++;

    return dirlist->cnt;
}

int
is_lowercase(char letter){
    return (letter >= 'A' && letter <= 'Z')?0:1;
}

char *
strip_terminating_questionmark(char *source){
    if(source == NULL) return NULL;
    int src_len = strlen(source);
    char *outcome;

    if(source[src_len - 1] == '?'){
        outcome = (char *) calloc(src_len, sizeof(char));
        strncpy(outcome, source, (src_len - 1));
        return outcome;
    }

    return source;
}

char *
hide_email(char *source){
    if(source == NULL) return NULL;

    size_t first = strcspn(source, "@");
    if(first == strlen(source)){ return source; }

    char *outcome = (char *) calloc(first + 2 + sizeof("HIDDEN"), sizeof(char));
    char *temp = (char *) calloc(first + 2 + sizeof("HIDDEN"), sizeof(char));
    strncpy(temp, source, first + 1);
    sprintf(outcome, "%sHIDDEN", temp);
    free(temp);
    return outcome;

}

char *
hide_email_hash(char *source){
    /* TODO: implement with CRC32 */
    return NULL;
}

int set_package_variable(Package *pkg, char *identifier, char *content){

    char *id = strip_terminating_questionmark(identifier);
    char *token;

    if(strcmp(id, "MAINTAINER") == 0){
        pkg->maintainer = trim(content);
        pkg->hidden_maintainer = hide_email(pkg->maintainer);
    } else if(strcmp(id, "PORTNAME") == 0){
        pkg->portname =  trim(content);
    } else if(strcmp(id, "LICENSE") == 0){
        pkg->license = trim(content);
    } else if(strcmp(id, "COMMENT") == 0) {
        pkg->comment = trim(content);
    } else if(strcmp(id, "CATEGORIES") == 0){
        while((token = strtok_r(content, " ", &content))){
            if(pkg->categories_cnt == pkg->categories_cap){
                /* no more space, need to resize */
                pkg->categories = (char **) reallocarray(pkg->categories, pkg->categories_cap * 2, sizeof(char *));
                pkg->categories_cap *= 2;
            }
            pkg->categories[pkg->categories_cnt] = trim(token);
            pkg->categories_cnt++;
        }
    } else if(strcmp(id, "LOG") == 0){
        while((token = strtok_r(content, " ", &content))){
            if(pkg->logs_cnt == pkg->logs_cap){
                /* no more space, need to resize */
                pkg->logs = (char **) reallocarray(pkg->logs, pkg->logs_cap * 2, sizeof(char *));
                pkg->logs_cap *= 2;
            }
            pkg->logs[pkg->logs_cnt] = trim(token);
            pkg->logs_cnt++;
        }
    } else if(strcmp(id, "URL") == 0){
        while((token = strtok_r(content, " ", &content))){
            if(pkg->urls_cnt == pkg->urls_cap){
                /* no more space, need to resize */
                pkg->urls = (char **) reallocarray(pkg->urls, pkg->urls_cap * 2, sizeof(char *));
                pkg->urls_cap *= 2;
            }
            pkg->urls[pkg->urls_cnt] = trim(token);
            pkg->urls_cnt++;
        }
    } else if(strcmp(id, "USERS") == 0){
        pkg->users = trim(content);
    } else if(strcmp(id, "DESCRIPTION") == 0){
        pkg->description = trim(content);
    } else if(strcmp(id, "GROUPS") == 0){
        pkg->groups = trim(content);
    } else if(strcmp(id, "BINFILES") == 0){
        pkg->binfiles = trim(content);
    } else if(strcmp(id, "PORTVERSION") == 0){
        pkg->portversion = trim(content);
    } else if(strcmp(id, "DISTVERSION") == 0){
        pkg->distversion = trim(content);
    } else if(strcmp(id, "USES") == 0){
        while((token = strtok_r(content, " ", &content))){
            if(pkg->uses_cnt == pkg->uses_cap){
                /* no more space, need to resize */
                pkg->uses = (char **) reallocarray(pkg->uses, pkg->uses_cap * 2, sizeof(char *));
                pkg->uses_cap *= 2;
            }
            pkg->uses[pkg->uses_cnt] = trim(token);
            pkg->uses_cnt++;
        }
    } else if(strcmp(id, "BROKEN") == 0){
        pkg->broken = trim(content);
    } else {
        /*
        printf("UNKNOWN VAR %s: %s", id, content);
        */
    }
    return 0;
}

void
free_FileContents(FileContents *fc){
    if(fc == NULL) return;
    for(int x = 0; x < fc->cnt; x++){ free(fc->lines[x]); }
    free(fc->lines);
    free(fc);
}
int
FileContents_add_line(FileContents *fc, char *line){
    char **ptr = NULL;

    if(fc->cnt == fc->cap){
        /* no more room: resize */
        ptr = (char **) reallocarray(fc->lines,fc->cap * 2,sizeof(char *));
        if(ptr == NULL) return 0;

        fc->cap *= 2;
        fc->lines = ptr;
    }

    fc->lines[fc->cnt] = line;
    fc->cnt++;

    return 1;
}
FileContents *
read_file_contents(char *path){
    int fd = open(path, 'r');
    if(fd < 0) return NULL;

    char *end = NULL, *start = NULL, *insert = NULL, *partial = NULL;
    ssize_t returned;
    size_t  tofetch = 15 * 1024;
    size_t length;
    char is_partial;

    FileContents *fc = calloc(sizeof(FileContents),sizeof(char));
    fc->cap = 500;
    fc->lines = calloc(fc->cap,sizeof(char*));

    /* 1 larger than what we fetch so string is always terminated,
     * even if partial */
    char *buf = malloc(tofetch + 1);
    do{
        memset(buf,0,tofetch + 1);
        returned = read(fd, buf, tofetch);
        /* if we have anything other than 0 then we have a continuing string */
        is_partial = 0;
        start = buf;
        do{
            end = strstr(start,"\n");
            if(end){
                length = (end - start) + 2;
            } else {
                length = strlen(start) + 1;
                is_partial = buf[tofetch];
            }
            if(is_partial){
                partial = calloc(length, sizeof(char));
                strncpy(partial, start, length);

            } else {
                if(partial != NULL){
                    /* join the previous and the current line */
                    insert = calloc(strlen(partial) + length, sizeof(char));
                    if(insert == NULL) return NULL;
                    strncpy(insert, partial, strlen(partial) + 1);
                    strncat(insert, start, length - 1);

                    /* zero out the previous line */
                    free(partial);
                    partial = NULL;
                } else {
                    insert = calloc(length, sizeof(char));
                    strncpy(insert, start, length - 1);

                }
                FileContents_add_line(fc, insert);

            }
            /* advance start to \n found */
            if(end != NULL && start < end){
                start = end;
            } 
            /* and move the pointer to the letter following it */
            start += 1;
            
        } while(end);


    } while(returned == tofetch);

    free(buf);
    close(fd);

    return fc;
}

int
parse_makefile(char *path, Package *pkg){
    int nread;
    char *line = NULL;
    char *equal = NULL;
    char *var = NULL;
    char *content = NULL;
    int con_len = 0;
    int var_len = 0;
    int prev_line_continues = 0;
    int curr_line_continues = 0;
    int in_variable = 0;

    FileContents *fc = read_file_contents(path);
    if(fc == NULL) return 0;

    for(int x = 0; x < fc->cnt; x++){
        line = fc->lines[x];
        nread = strlen(line);
        /* if we didn't have a previous line continuator claiming this
         * line: reset the 'are we in a variable' state */
        if(!prev_line_continues) in_variable = 0;
        curr_line_continues = (line[nread - 2] == '\\')?1:0;
        equal = line;
        /* if it starts with uppercase then it might be a variable */
        if(!is_lowercase(line[0])){
            /* check for presence of = */
            equal = strchr(line, '=');
            /* we found = so assume we have a variable */
            if(equal){
                /* fill in the char with 0 to split the 2 strings
                 * line will then point to the first variable name part
                 * we augment the pointer of equal by 1 so it'll point to
                 * the content  */
                in_variable = 1;
                *equal = 0;
                equal++;
                var_len = strlen(line) + 1;
                con_len = strlen(equal) + 1;


                var = calloc(var_len, sizeof(char));
                snprintf(var,var_len, "%s", line);
                /* fill in the content but strip the multi-line continuator
                 * and the carriage return if it's a continuation */
                content = calloc(con_len, sizeof(char));
                strncat(content, equal, curr_line_continues?(con_len - 3):(con_len - 1) );
            } else { 
                /* we reset equal to the start of line because 
                 * we use it later in a context where we need to get
                 * the string data either partly or in full and we eliminate
                 * comparisons this way (see prev_line_continues just below) */
                equal = line;
            }
        }

        if(prev_line_continues){
            /* grow memory and concat:
             *
             * length of previous content +
             * length this content - 2 ('\\n') if current line continues +
             * 1 (final terminating 0)*/
                content = reallocarray(content, (strlen(content) + ((strlen(equal) - (curr_line_continues?2:0))) + 1), sizeof(char));
            content = strncat(content, equal, strlen(equal) - (curr_line_continues?2:0));
        }
        /* we're at the end of the (multi)line, assign content */
        if(!curr_line_continues){
            if(in_variable){
                set_package_variable(pkg,var,content);
            }

        }

        prev_line_continues = (line[nread - 2] == '\\')?1:0;
    }
    free_FileContents(fc);
    return 1;
}

int
parse_pkg_descr(char *path, Package *pkg){
    FileContents *fc;
    char *line = NULL;
    char *www = NULL;
    char *description = NULL;
    int success = 0;

    fc = read_file_contents(path);
    if(fc == NULL) return 0;

    success = 1;
    for(int x = 0; x < fc->cnt; x++){
        line = fc->lines[x];
        /* is the line a WWW: url then extract and append to urls*/
        if((www = strstr(line, "WWW:"))){
            set_package_variable(pkg, "URL", (www + 5));
        } else {
        /* otherwise append to description */
            if(description){
                description = (char *) reallocarray(description, strlen(description) + strlen(line) + 1, sizeof(char));
            } else {
                description = (char *) calloc(strlen(line) + 1, sizeof(char));
            }
            if(description == NULL){
                success = 0;
                break;
            }
            description = strncat(description, line, strlen(line));
        }
    }
    if(description){
        set_package_variable(pkg, "DESCRIPTION", description);
        free(description);
    }

    free_FileContents(fc);
    return success;
}

char *
ltrim(char *source){
    if(source == NULL) return NULL;
    int src_len = strlen(source);
    char *outcome;
    int out_len = 0;
    for(int x = 0;x < src_len;x++){
        /* check for ascii SPACE (32), CR (13), LF (10) and TAB (9) */
        if(*source == 32 || *source == 9 || *source == 10
                || *source == 13){
            /* found it so move to next character */
            source++;
        } else {
            /* we found the first non-space, non-tab character */
            out_len = strlen(source) + 1;
            outcome = calloc(out_len, sizeof(char));
            snprintf(outcome, out_len, "%s", source);

            return outcome;
        }
    }

    return NULL;
}

char *
rtrim(char *source){
    if(source == NULL) return NULL;
    int src_len = strlen(source);
    char *outcome;
    for(int x = src_len; x > 0; x--){
        if(source[x - 1] != 32 && source[x - 1] != 9 &&
                source[x - 1] != 10 && source[x - 1] != 13){
            /* last non-whitespace character found */
            outcome = (char *) calloc(x + 1, sizeof(char));
            strncpy(outcome, source, x);
            return outcome;
        }
    }
    return NULL;
}

char *
trim(char *source){
    if(source == NULL) return NULL;

    return rtrim(ltrim(source));
}
