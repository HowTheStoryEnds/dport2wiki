#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>

#define CAT_BASE 50
#define PKG_BASE 1000
#define LST_BASE 10


typedef struct{
    char **lines;
    int cnt;
    int cap;
} FileContents;

struct Package{
    char *name;
    char *portname;
    char *maintainer;
    char *hidden_maintainer;
    char *hashed_maintainer;
    char *version;
    char *portversion;
    char *distversion;
    char *previous_version;
    char *current_version;
    char *comment;
    char *description;
    char *users;
    char *groups;
    char *binfiles;
    char *broken;
    char *license;
    
    char **urls;
    int urls_cnt;
    int urls_cap;
    
    char **logs;
    int logs_cnt;
    int logs_cap;
    
    char **uses;
    int uses_cnt;
    int uses_cap;
    
    char **categories;
    int categories_cnt;
    int categories_cap;
};
typedef struct Package Package;

struct Category{
    char *name;
    Package **packages;
    int packages_cnt;
    int packages_cap;
};
typedef struct Category Category;

struct Directory{
    char *name;
    char *path;
};
typedef struct Directory Directory;

struct DirList{
    int cnt;
    int cap;
    Directory** directories;
};
typedef struct DirList DirList;

int is_lowercase(char letter);
int parse_makefile(char *path, Package *pkg);
int parse_pkg_descr(char *path, Package *pkg);
int get_directories(char *path, DirList *result);
int push_to_dirlist(DirList *container, Directory *);
DirList * new_DirList();
void free_DirList(DirList *dl);
Directory * new_Directory();
Directory * new_Directory_using(char *path, char *name);
void free_Directory(Directory *dir);
Package * new_Package();
void free_Package(Package *pkg);
Category * new_Category();
void free_Category(Category *cat);
int category_add_package(Category *category, Package *package);
char * combine_paths(char *one, char *two);
char * ltrim(char *);
char * rtrim(char *);
char * trim(char *);
int set_package_variable(Package *pkg, char *id, char *content);
char * strip_terminating_questionmark(char *source);
char * hide_email(char *source);
char * hide_email_hash(char *source);
int FileContents_add_line(FileContents *, char *);
FileContents * read_file_contents(char *);
void free_FileContents(FileContents *);
