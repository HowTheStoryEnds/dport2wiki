#include "minunit.h"
#include "dport2mdwn.h"

MU_TEST(test_is_lowercase) {
	mu_check(is_lowercase('a') == 1);
	mu_check(is_lowercase('p') == 1);
	mu_check(is_lowercase('z') == 1);
    mu_check(is_lowercase('Z') == 0);
    /* anything that's not a letter gets a pass */
    mu_check(is_lowercase('1') == 1);
}
MU_TEST(test_combine_paths) {
    char *buf = combine_paths("/home", "Harald");
    mu_check(buf != NULL);
    mu_assert_string_eq(buf, "/home/Harald");
    char *buf2 = combine_paths(buf, "test/aap");
    mu_check(buf2 != NULL);
    mu_assert_string_eq("/home/Harald/test/aap", buf2);
    free(buf2);
    buf2 = combine_paths(buf, "/test/aap");
    mu_check(buf2 != NULL);
    mu_assert_string_eq("/home/Harald/test/aap", buf2);
    free(buf2);
    free(buf);
}

MU_TEST(test_push_null_to_dirlist) {
    DirList *dl = new_DirList();
    Directory *d = NULL;

    mu_check(push_to_dirlist(dl,d) == 0);
    free_DirList(dl);

}

MU_TEST(test_push_to_dirlist) {
    DirList *dl = new_DirList();
    Directory *d = new_Directory_using("name", "path");

    mu_check(push_to_dirlist(dl,d) == 1);
    mu_check(dl->cnt == 1);
    mu_check(dl->cap == LST_BASE);
    free_DirList(dl);
}


MU_TEST(test_get_directories) {
    DirList *dl = new_DirList();
    get_directories("./tester", dl);
    mu_assert_int_eq(3, dl->cnt);
    free_DirList(dl);
}

MU_TEST(test_new_Directory) {
    Directory *d = new_Directory();
    mu_check(d != NULL);
    mu_check(d->name != NULL);
    mu_check(d->name[0] == 0);
    mu_check(d->path != NULL);
    mu_check(d->path[0] == 0);
    free_Directory(d);
}


MU_TEST(test_new_Directory_using) {
    Directory *d = new_Directory_using("Harold", "/home/Harold");
    mu_check(d != NULL);
    mu_assert_string_eq(d->name, "Harold");
    mu_assert_string_eq(d->path, "/home/Harold");
    free_Directory(d);
}

MU_TEST(test_create_new_DirList) {
    DirList *dl = new_DirList();
    mu_check(dl != NULL);
    mu_check(dl->cap = LST_BASE);
    mu_check(dl->cnt == 0);
    mu_check(dl->directories != NULL);
    free_DirList(dl);
}
MU_TEST(test_parse_makefile){
    Package *pkg = new_Package();
    mu_check(pkg);
    mu_assert_int_eq(0, parse_makefile("./doesnotexist/Makefile", pkg));
    mu_assert_int_eq(1, parse_makefile("./tester/Makefile", pkg));
    mu_assert_string_eq("1.16.0", pkg->portversion);
    mu_assert_string_eq("nginx", pkg->portname);
    mu_assert_int_eq(1, pkg->categories_cnt);
    mu_assert_string_eq("www", pkg->categories[0]);
    mu_assert_string_eq("BSD2CLAUSE", pkg->license);
    mu_assert_int_eq(1, pkg->uses_cnt);
    mu_assert_string_eq("cpe", pkg->uses[0]);
    mu_assert_string_eq("Robust and small WWW server", pkg->comment);
    free_Package(pkg);
}
MU_TEST(test_parse_pkg_descr){
    char *description = "NGINX is a high performance edge web server with the lowest memory footprint\nand the key features to build modern and efficient web infrastructure.\n\nNGINX functionality includes HTTP server, HTTP and mail reverse proxy, caching,\nload balancing, compression, request throttling, connection multiplexing and\nreuse, SSL offload and HTTP media streaming.";
    Package *pkg = new_Package();
    mu_check(pkg);
    mu_assert_int_eq(0, parse_pkg_descr("./doesnotexist/pkg-descr", pkg));
    mu_assert_int_eq(1, parse_pkg_descr("./tester/pkg-descr", pkg));
    mu_assert_int_eq(2, pkg->urls_cnt);
    mu_assert_string_eq("https://nginx.org/",pkg->urls[0]);
    mu_assert_string_eq("https://nginx.com/",pkg->urls[1]);
    mu_assert_string_eq(description, pkg->description);
    free(pkg);
}
MU_TEST(test_ltrim){
    mu_assert_string_eq("harald",ltrim("\tharald"));
    mu_assert_string_eq("harald",ltrim("           harald"));
    mu_assert_string_eq("harald",ltrim("  \t    \t     harald"));
    mu_assert_string_eq("harald",ltrim("  \t  \r\n  \t     harald"));
    mu_assert_string_eq("harald",ltrim("  \t   \n \t  \n   harald"));
    mu_assert_string_eq("harald",ltrim("  \t \n   \t     \nharald"));
}
MU_TEST(test_rtrim){
    mu_assert_string_eq("harald", rtrim("harald    "));
    mu_assert_string_eq("  harald", rtrim("  harald    "));
    mu_assert_string_eq("\t  harald", rtrim("\t  harald    "));
    mu_assert_string_eq("harald", rtrim("harald   \t \r\n"));
    mu_assert_string_eq("harald", rtrim("harald   \t \r\n\n"));
}
MU_TEST(test_trim){
    mu_assert_string_eq("harald", trim(" harald "));
    mu_assert_string_eq("harald", trim(" harald \t\n"));
    mu_assert_string_eq("harald", trim(" harald "));
    mu_assert_string_eq("harald", trim(" harald "));
    mu_assert_string_eq("harald", trim(" harald "));
    mu_assert_string_eq("harald", trim(" harald "));

}
MU_TEST(test_set_package_variable){
    Package *pkg = new_Package();
    mu_check(pkg != NULL);
    set_package_variable(pkg,"MAINTAINER","joneum@FreeBSD.org");
    mu_assert_string_eq("joneum@FreeBSD.org", pkg->maintainer);
    mu_assert_string_eq("joneum@HIDDEN", pkg->hidden_maintainer);
    /* TODO: create hashed maintainer
    mu_assert_string_eq("joneum_AT_23c241d7", pkg->hashed_maintainer);
    */
    set_package_variable(pkg,"PORTNAME","some_package \n");
    mu_assert_string_eq("some_package", pkg->portname);
    set_package_variable(pkg,"LICENSE","GPL \n");
    mu_assert_string_eq("GPL", pkg->license);
    set_package_variable(pkg,"COMMENT","SOMETHING \n");
    mu_assert_string_eq("SOMETHING", pkg->comment);
    set_package_variable(pkg,"PORTVERSION","1.16.0");
    mu_assert_string_eq("1.16.0", pkg->portversion);
    set_package_variable(pkg,"USERS","1.16 .0");
    mu_assert_string_eq("1.16 .0", pkg->users);
    set_package_variable(pkg,"GROUPS","1. 16 .0");
    mu_assert_string_eq("1. 16 .0", pkg->groups);
    set_package_variable(pkg,"BINFILES","files");
    mu_assert_string_eq("files", pkg->binfiles);
    set_package_variable(pkg,"PORTVERSION","1.16.0");
    mu_assert_string_eq("1.16.0", pkg->portversion);
    set_package_variable(pkg,"DISTVERSION",".10");
    mu_assert_string_eq(".10", pkg->distversion);
    set_package_variable(pkg,"BROKEN","You're poison. Poison running..");
    mu_assert_string_eq("You're poison. Poison running..", pkg->broken);
    
    char * test = malloc(sizeof("multimedia devel\n") + 1);
    sprintf(test, "multimedia devel\n");
    set_package_variable(pkg, "CATEGORIES",test);
    mu_assert_int_eq(2, pkg->categories_cnt);
    mu_assert_string_eq("multimedia", pkg->categories[0]);
    mu_assert_string_eq("devel", pkg->categories[1]);
    free(test);
    
    test = malloc(sizeof("multimedia devel\n") + 1);
    sprintf(test, "multimedia devel\n");
    set_package_variable(pkg, "LOG",test);
    mu_assert_int_eq(2, pkg->logs_cnt);
    mu_assert_string_eq("multimedia", pkg->logs[0]);
    mu_assert_string_eq("devel", pkg->logs[1]);
    free(test);

    test = malloc(sizeof("multimedia devel\n") + 1);
    sprintf(test, "multimedia devel\n");
    set_package_variable(pkg, "URL",test);
    mu_assert_int_eq(2, pkg->urls_cnt);
    mu_assert_string_eq("multimedia", pkg->urls[0]);
    mu_assert_string_eq("devel", pkg->urls[1]);
    free(test);

    
    test = malloc(sizeof("multimedia devel\n") + 1);
    sprintf(test, "multimedia devel\n");
    set_package_variable(pkg, "USES",test);
    mu_assert_int_eq(2, pkg->uses_cnt);
    mu_assert_string_eq("multimedia", pkg->uses[0]);
    mu_assert_string_eq("devel", pkg->uses[1]);
    
    free(test);
    free_Package(pkg);
}
MU_TEST(test_strip_terminating_questionmark){
    mu_assert_string_eq("harald", strip_terminating_questionmark("harald"));
    mu_assert_string_eq("harald", strip_terminating_questionmark("harald?"));
    mu_assert_string_eq("harald?", strip_terminating_questionmark("harald??"));
    mu_assert_string_eq("har?ald", strip_terminating_questionmark("har?ald?"));
}

MU_TEST(test_hide_email){
    mu_assert_string_eq("harald@HIDDEN", hide_email("harald@gmail.com"));
    mu_assert_string_eq("harald.brinkhof@HIDDEN", hide_email("harald.brinkhof@gmail.co.uk"));
    mu_assert_string_eq("harald.brinkhof+tag@HIDDEN", hide_email("harald.brinkhof+tag@gmail.co.uk"));
    mu_assert_string_eq("harald", hide_email("harald"));

}
MU_TEST(test_category_add_package){
    Category *c = new_Category();
    Package *p = new_Package();
    mu_check(p && c);
    mu_assert_int_eq(1, category_add_package(c, p));
    mu_assert_int_eq(1, c->packages_cnt);
    mu_assert_int_eq(0, category_add_package(NULL, p));
    mu_assert_int_eq(0, category_add_package(c, NULL));
    mu_assert_int_eq(1, c->packages_cnt);
    
    for(int x = 0; x < (PKG_BASE + 1);x++){
        category_add_package(c, new_Package());
    }
    mu_assert_int_eq(PKG_BASE + 2, c->packages_cnt);
    mu_assert_int_eq(PKG_BASE * 2, c->packages_cap);
    free_Category(c);
}

MU_TEST_SUITE(test_suite) {
	MU_RUN_TEST(test_is_lowercase);
	MU_RUN_TEST(test_combine_paths);
	MU_RUN_TEST(test_push_null_to_dirlist);
	MU_RUN_TEST(test_push_to_dirlist);
	MU_RUN_TEST(test_get_directories);
	MU_RUN_TEST(test_create_new_DirList);
	MU_RUN_TEST(test_new_Directory);
	MU_RUN_TEST(test_new_Directory_using);
	MU_RUN_TEST(test_parse_makefile);
	MU_RUN_TEST(test_parse_pkg_descr);
    MU_RUN_TEST(test_ltrim);
    MU_RUN_TEST(test_rtrim);
    MU_RUN_TEST(test_trim);
    MU_RUN_TEST(test_set_package_variable);
    MU_RUN_TEST(test_strip_terminating_questionmark);
    MU_RUN_TEST(test_hide_email);
    MU_RUN_TEST(test_category_add_package);
}

int main(int argc, char *argv[]) {
	MU_RUN_SUITE(test_suite);
	MU_REPORT();
	return minunit_status;
}
