
package P2WFileOps;

use strict;
use warnings;
use DebugPrint;
use String::CRC32;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(makedir loadfile trysave savefile);

# FILESYSTEM-HANDLING FUNCTIONS
{ # block-wrapped to keep %dirscreated static and private to makedir()
my %dirscreated;
# makedir(directory)
sub makedir{
    if(!exists $dirscreated{$_[0]}){
        $dirscreated{$_[0]} = undef;
        if(!(-e $_[-1] and -d $_[0])){
            mkdir($_[0]);
        }
        debugprint("creating directory: $_[0]\n",'1:CREATE_DIR:FILE_OPS');
    } else { debugprint("directory already created: $_[0]\n",'5:CREATE_DIR:FILE_OPS'); }
};
}

# loadfile(filename)
sub loadfile{
    debugprint("opening file: $_[0]\n",'3:OPEN_FILE:FILE_OPS');
    local $/ = undef;
    open my $fh, "<:unix", $_[0]
        or die "could not open $_[0]: $!";
    return <$fh>;
};

# determines wether we need to save this file
# trysave(filename, content, config)
# uses savefile
sub trysave{
    my $inputchecksum = crc32($_[1]);
    # do nothing if the file already exists and its content is identical
    if(-e $_[0] and ($inputchecksum == crc32(loadfile($_[0])) or lc($_[2]->{'overwrite'}) eq 'none') and lc($_[2]->{'overwrite'}) ne 'all' ){
        debugprint("not saving file: $_[0]\n",'3:NOT_SAVING_FILE:FILE_OPS');
        return;
    }

    savefile($_[0], $_[1]);
}
# savefile(filename, content)
sub savefile{
    debugprint("saving file: $_[0]\n",'3:SAVE_FILE:FILE_OPS');
    open(my $fh, '>', $_[0]) or die "Could not open file for writing: '$_[0]' $!";
    print $fh $_[1];
    close $fh;
};

1;
