package DebugPrint;

use strict;
use warnings;
use Scalar::Util qw(looks_like_number);

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(debugprint);

# adjust debuglevel to see varying levels of info printed during run
# use this format NOISE_LEVEL:ACTIVITY_NAME:GROUP_NAME
# NOISE_LEVEL: always needs to be high enough
# ACTIVITY_NAME: narrow it down to a specific ACTIVITY (accepts 'ALL')
# GROUP_NAME: show all activities from a specific subgroup (accepts 'ALL')
#
# -1:   total silence
# 0:    useful info (packages processed)
# 1:    unknown tags processed
# 5:    lots of info printed out, displaying state of internal variables

our $debuglevel = '-1:NONE:NONE'; # global value

# debuglevel level:name:group
# debugprint(string_to_print, debug_level)
sub debugprint{
    my @levels = split /:/, $_[1];
    my @dbglvl = split /:/, $debuglevel;

    my $canprint = 0;
    if(looks_like_number($dbglvl[0]) and $levels[0] <= $dbglvl[0]){
        if($levels[1] eq $dbglvl[1] or $dbglvl[1] eq 'ALL'){
            $canprint += 1;
        }
        if($levels[2] eq $dbglvl[2] or $dbglvl[2] eq 'ALL'){
            $canprint += 1;
        }
    }
    if($canprint > 0){
        print $_[0];
    }
};

1;
