<%= $name %> - <%= $short_description %>
=============================
 ## Description
 <%= $description %>\n
 ## Specifics
 Maintained by: <%= $hidden_maintainer %>\n
 License: <%= $license %>\n
 Uses: <%# $uses %>\n
 Depends: <%# $depends %>\n
 WWW: <%# $urls %>\n

