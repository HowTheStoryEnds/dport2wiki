<%= $header %>

% foreach my $entry(@{$list}){
* [[<%= $entry %>]]
%}

% if($previous ne ''){
[[previous| <%= $previous %>]]
%}

% if($next ne ''){
[[next| <%= $next %>]]
%}
