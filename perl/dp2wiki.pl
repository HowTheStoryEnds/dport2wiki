#!/usr/bin/perl

use strict;
use warnings;
# load perl module in current dir #
use Cwd qw( abs_path );           #
use File::Basename qw( dirname ); #
use lib dirname(abs_path($0));    #
###################################
use File::Find::Rule;
use File::Basename;
use Data::Dumper;
use File::Spec;
use DateTime;
use Storable;
use DebugPrint;
use P2WFileOps;
use P2WDataOps;


#  Get list of all packages
#  if clean install:
#           get list of all packages
#           load package data
#           create directory if not exist
#           write package file
#           gather from scratch and write by_* lists
#           store by_* lists
#
#   if recurring run:
#           determine packages with changes
#           load package data
#           create directory if not exist
#           write package file
#           if new package file:
#                   load by_* lists
#                   rewrite by_* lists
#                   store changed by_* lists





# RUNNING CODE STARTS HERE
$P2WDataOps::config->{'input'} = '/home/harald/DPorts';
$P2WDataOps::config->{'output'} = '/home/harald/dport_to_wiki/output';
#$P2WDataOps::config->{'output'} = '/home/harald/dflybsd_site/ikiwiki/ports';
#$DebugPrint::debuglevel = "1:ALL:ALL";

#my $dports = Git->repository(Directory => $P2WDataOps::config->{'input'});
#my @changed_files = $dports->command('diff','--name-status','HEAD~2','HEAD');
#my @changed = getListOfChangedPackages();
#print renderTemplate('page', loadPackageData($changed[0]));
#print Dumper(@grepfiles);
#exit;
#print Dumper(getAvailablePorts());
#exit;

#my $revisions = getLastRevisions();

#my ($bycategory, $byalphabet, $bymaintainer, $maintainerports, $packages) = parseAllPorts();
#print Dumper(P2WDataOps->GetPackageInfoFromGit());

#print Dumper(listByMaintainerFromGit('culot@freebsd.org','vmagerya@gmail.com'));
my ($packages, $byAlphabet, $byCategory, $byMaintainer, $pkgsByMaintainer) = GetPackageInfoFromGit();
my $revisions = getLastRevisions();
addRevisionsToPackages($packages, $revisions);
#print Dumper($packages);
#savePackageFiles($packages, 'true');
exit(1);

my $changedPackages = getListOfChangedPackages();
foreach my $key(keys %{$changedPackages}){
    my $status = $changedPackages->{$key};
    if($status eq 'A'){ # new package
    
    }
    elsif($status eq 'M'){ # modified package
        print "GetPackageInfoFromGit([$key])\n";
        my ($packages, $byAlphabet, $byCategory, $byMaintainer, $pkgsByMaintainer) = GetPackageInfoFromGit([$key]);
        print "getLastRevisions([$key])\n";
        my $revisions = getLastRevisions([$key]);
        print "addRevisionsToPackages()\n";
        addRevisionsToPackages($packages, $revisions);
        print "savePackageFiles()\n";
        savePackageFiles($packages);
        print "MODIFIED: $key";
        print Dumper($packages);


    }
    elsif($status eq 'D'){ # deleted package
    }
    else{
    
    }
    print "PKG: $key STATE: $changedPackages->{$key}\n";
}
#addRevisionsToPackages($packages, $revisions);
#print Dumper($packages);
#generateSubsections($byAlphabet,'by_alphabet_',$P2WDataOps::config);
#generateSubsections($byCategory, 'by_category_', $P2WDataOps::config);
#generateSubsections($byMaintainer, 'by_maintainer_', $P2WDataOps::config);
#generateSubsections($pkgsByMaintainer, 'pkgs_by_', $P2WDataOps::config, 'false');
#generateSection($byAlphabet, 'by_alphabet_');
#generateSection($byCategory, 'by_category_');
#generateSection($byMaintainer, 'by_maintainer_');
exit 1;
