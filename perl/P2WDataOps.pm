package P2WDataOps;

use strict;
use warnings;
use DebugPrint;
use File::Spec;
use P2WFileOps;
use String::CRC32;
use Mojo::Template;
use Git;
use Data::Dumper;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(loadPackageData getListOfChangedPackages renderTemplate
                getLastRevisions getAvailablePorts listByMaintainerFromGit
                addRevisionsToPackages savePackageFiles generateSubsections
                generateSection GetPackageInfoFromGit);


our $config = {
    'input' => '',
    'output' => '',
    'overwrite' => 'newer', # newer/all/none
    'per_page' => 100,
    'git_logs_to_show' => 3,
    'templates' => {'page' => 'page.mt',
                    'section' => 'section.mt',
                    'subsection' => 'subsection.mt'}
};

sub revision_t(){ return{
    'timestamp' => 0,
    'hash' => '',
    'author' => '',
    'date' => '',
    'subject' => '',
    'version_parsed' => '',
    'package' => ''
    }

}
sub package_t(){ return {
    'maintainer' => '',
    'hidden_maintainer' => '',
    'license' => '',
    'name' => '',
    'category' => '',
    'version' => '',
    'distversion' => '',
    'short_description' => '',
    'description' => '',
    'dependencies' => [],
    'url' => [],
    'categories' => [],
    'users' => [],
    'groups' => [],
    'bin_files' => [],
    'uses' => [],
    'broken' => [],
    'broken_arch' => [],
    'broken_feature' => [],
    'hashed_maintainer' => '',
    'status' => '',
    'current_version' => '',
    'previous_version' => '',
    'path' => '',
    'git_logs' => []
    };
};

# generateSubsections(subsection_dictionary, prefix, config, generate_header)
# returns first file generated
sub generateSubsections{
    my @list = (); # generic holding array
    my $prefix = $_[1];
    my $header = '';
    my $config = $_[2];
    my $ss = {  'list' => \@list,
                'header' => '',
                'previous' => '',
                'next' => ''};

    #generate the header
    if(!exists $_[3] or $_[3] eq 'true'){
        foreach my $key(sort keys %{$_[0]}){
            push @list, "[[$key|${key}_0]]";
        }
        $ss->{'header'} = join(' - ', @list);
    }
    foreach my $key(sort keys %{$_[0]}){
        my $counter = 0;
        my $page = 0;
        my $nextpage = '';
        my $prevpage = '';
        @list = ();
        foreach my $entry(sort keys %{%{$_[0]}{$key}}){
            push @list, $entry;
            if(scalar @list == $config->{'per_page'}){
                $ss->{'previous'} = '';
                $ss->{'next'} = '';

                if($page > 0){ 
                $prevpage = '[<previous](../' . $key . '_' . ($page - 1) . ')'; 
                $ss->{'previous'} = $key . '_' . ($page - 1);
                }
                $nextpage = '[next>](../' . $key . '_' . ($page + 1) . ')';
                $ss->{'next'} = $key . '_' . ($page + 1);
                my $content = $header . "\n" . join("\n", @list) . "\n" . "$prevpage  $nextpage";
                # keep first file's name to return
                my $template = renderTemplate('subsection', $ss);
                trysave(File::Spec->catfile($config->{'output'}, $prefix . $key . '_' . $page . '.mdwn'),$template, $config);
                #trysave(File::Spec->catfile($config->{'output'}, $prefix . $key . '_' . $page . '.mdwn'),$content, $config);
                @list = ();
                $page++;
            }

        }
        if(scalar @list > 0){
                $prevpage = '[<previous](../' . $key . '_' . ($page - 1) . ')';
                my $content = $header. "\n" . join("\n", @list) . "\n" . "$prevpage";
                #trysave(File::Spec->catfile($config->{'output'}, $prefix . $key . '_' . $page . '.mdwn'),$content, $config);
                my $template = renderTemplate('subsection', $ss);
                trysave(File::Spec->catfile($config->{'output'}, $prefix . $key . '_' . $page . '.mdwn'),$template, $config);
        }
    }

}

sub generateSection{
    my @entries = sort keys %{$_[0]};
    my $list = {'entries' => \@entries};
    my $name = $_[1];
    my $content = renderTemplate('section', $list);
    trysave(File::Spec->catfile($config->{'output'}, $name . 'section.mdwn'),$content, $config);
    
}

# savePackageFiles(packages, clean)
sub savePackageFiles{
    foreach my $pkg(keys %{$_[0]}){
        makedir(File::Spec->catdir($config->{'output'}, $_[0]->{$pkg}->{'category'}));
        my $page = renderTemplate('page', $_[0]->{$pkg});
        if(defined $_[1] and $_[1] eq 'true'){
            savefile(File::Spec->catfile($config->{'output'}, "$_[0]->{$pkg}->{'category'}/$_[0]->{$pkg}->{'name'}.mdwn"), $page);
        } else {
            trysave(File::Spec->catfile($config->{'output'}, "$_[0]->{$pkg}->{'category'}/$_[0]->{$pkg}->{'name'}.mdwn"), $page, $config);
        }
    }
}

# renderTemplate(templateName, data)
my %templatesLoaded = ();
sub renderTemplate{
    my $mt = Mojo::Template->new(vars => 1);
    my $template = '';
    if(!defined $templatesLoaded{$_[0]}){
        if(!defined $config->{'templates'}{$_[0]}){
            die("template file not available: $_[0]\n");
        }
        $templatesLoaded{$_[0]} = loadfile($config->{'templates'}{$_[0]}); 
    }
        $template = $templatesLoaded{$_[0]};

    return $mt->render($template, $_[1] );
}

# trim whitespace from both ends
sub trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

sub getListOfChangedPackages{
    my $dp = Git->repository(Directory => $config->{'input'});
    my @result = $dp->command('diff', '--no-color', '--name-status', '-G', 'PORTVERSION=', 'HEAD~2', 'HEAD');
    #my @filtered = grep(/^diff --git a\//, @result);
    my %found;
    foreach my $line(@result){
        my @var = split(/\s/, $line =~ s/\/Makefile$//r);
        $found{$var[1]} = $var[0];
    }

    return \%found;

}

# getParseDataFromGit(list_of_packages)
sub getParseDataFromGit{
    my $limiter;
    if(defined $_[0]){
        $limiter = join(' ', @{$_[0]});
    } else {
        $limiter = '[a-z]*';
    }
    my $dp = Git->repository(Directory => $config->{'input'});
    my @result = $dp->command('grep', '-i', '-P',  '--cached', '--max-depth', '1', '.*', '--', $limiter);
    #my @result = $dp->command('grep', '-i', '-P',  '--cached', '--max-depth', '1', '[A-Z]+=\s', '--', $limiter);
    return \@result;
}
sub listByMaintainerFromGit{
    my $dp = Git->repository(Directory => $config->{'input'});
    my @result = $dp->command('grep', '-i', '-P',  '--cached', '--max-depth', '1', 'MAINTAINER=\s' . join('|',@_), '--', '[a-z]*');
    my %pkgs = ();
    foreach my $line(@result){
        my ($pkg, $option, $value) = parseGitLineForMake($line);
        if(!defined $pkg or !defined $option or !defined $value){
            # so move on
            next;
        }
        if(!defined $pkgs{$value}){ $pkgs{$value} = () }
        push @{$pkgs{$value}}, $pkg;
    }
    return \%pkgs;

}
sub parseGitLineForMake{
    my @result = $_[0] =~ /^(.*)\/Makefile:([^=]+)=\s(.*)$/;
    return @result;
}
sub parseGitLineForPkgdescr{
    my @result = $_[0] =~ /^(.*)\/pkg-descr:(.*)$/;
    return @result;

}
sub firstLetterLC{
    return lc(substr($_[0],0,1));
}

sub GetPackageInfoFromGit{
    my %packages = ();
    my %byAlphabet = ();
    my %byCategory = ();
    my %byMaintainer = ();
    my %pkgsByMaintainer = ();

    # get all the option lines from the Makefiles known to git
    foreach my $line(@{getParseDataFromGit($_[0])}){
        my $descr = undef;
        my ($pkg, $option, $value) = parseGitLineForMake($line);
        # if the line doesn't parse properly, it's not one we're interested in
        if(!defined $pkg or !defined $option or !defined $value){
            # not a makefile line, maybe it was a pkg-descr
            ($pkg, $descr) = parseGitLineForPkgdescr($line);
            if(!defined $descr){
                next;
            }
        }
        # make sure we actually have a package class in the dictionary in fill
        # in its name and main category
        if(!defined $packages{$pkg}){
            $packages{$pkg} = package_t();
            my($category, $pkgName) = split(/\//, $pkg);
            $packages{$pkg}->{'category'} = $category;
            $packages{$pkg}->{'name'} = $pkgName;
            my $fl = firstLetterLC($pkgName);
            if(!defined $byAlphabet{$fl}){
                $byAlphabet{$fl} = {};
            }
            $byAlphabet{$fl}{$pkg} = 1;
            $byCategory{$category}{$pkg} = 1;

        }

        if(defined $descr){
            # descriptions sometimes contain links
            if ($descr =~ /WWW: .*/){
                my @url = split(/ /, $descr); # extract URL
                push @{$packages{$pkg}->{'url'}}, $url[-1];
            } else {
                $packages{$pkg}->{'description'} .= "$descr\n";
            }
        }

        if(defined $option){
        # react on whatever option it is that we found
            if    ($option eq 'MAINTAINER'){
                # unaltered maintainer email
                $packages{$pkg}->{'maintainer'} = trim($value);
                # used to write a unique file and not expose the address in the url
                $packages{$pkg}->{'hashed_maintainer'} = hideEmail(trim($value), '_AT_' . crc32(trim($value)));
                # email hidden used for visual display
                $packages{$pkg}->{'hidden_maintainer'} = hideEmail(trim($value), '@HIDDEN'); 
                #my $fl = firstLetterLC($packages{$pkg}->{'maintainer'});
                #if(!defined $byMaintainer{$fl}){
                #    $byMaintainer{$packages{$pkg}->{'maintainer'}} = {};
                #}
                #if(!defined $byMaintainer{$fl}{$packages{$pkg}->{'maintainer'}}){
                #    $byMaintainer{$fl}{$packages{$pkg}->{'maintainer'}} = {};
                #}
                #$byMaintainer{$fl}{$packages{$pkg}->{'maintainer'}}{$pkg} = 1;
                $byMaintainer{$packages{$pkg}->{'hashed_maintainer'}}{$pkg} = 1;
                $pkgsByMaintainer{$packages{$pkg}->{'hashed_maintainer'}}{$pkg} = 1;
            }
            elsif ($option eq 'LICENSE'){ $packages{$pkg}->{'license'} = trim($value); }
            elsif ($option eq 'COMMENT'){ $packages{$pkg}->{'short_description'} = trim($value); }
            elsif ($option eq 'CATEGORIES'){ @{$packages{$pkg}->{'categories'}} = split(/ /, trim($value));}
            elsif ($option eq 'USERS'){ @{$packages{$pkg}->{'users'}} = split(/ /, trim($value)) ;}
            elsif ($option eq 'GROUPS'){ @{$packages{$pkg}->{'groups'}} = split(/ /, trim($value));}
            elsif ($option eq 'BINFILES'){ @{$packages{$pkg}->{'bin_files'}} = split(/ /, trim($value));}
            elsif ($option eq 'PORTVERSION'){ $packages{$pkg}->{'version'} = trim($value); }
            elsif ($option eq 'DISTVERSION'){ $packages{$pkg}->{'distversion'} = trim($value); }
            elsif ($option eq 'USES'){ @{$packages{$pkg}->{'uses'}} = split(/ /, trim($value));}
            elsif ($option eq 'BROKEN'){ push @{$packages{$pkg}->{'broken'}}, trim($value);}
            elsif ($option =~ /^BROKEN_/){ push @{$packages{$pkg}->{'broken_arch'}}, ($option =~ s/^BROKEN_//r, trim($value)); }
            elsif ($option =~ /_BROKEN$/){ push @{$packages{$pkg}->{'broken_feature'}}, ($option =~ s/_BROKEN$//r, trim($value)); }
            else{ debugprint("UNHANDLED VAR: $option value $value\n", '1:UNHANDLED_VARIABLE:PARSE_DATA'); }
        }
    }

    return \%packages, \%byAlphabet, \%byCategory, \%byMaintainer, \%pkgsByMaintainer;
}

# addRevisionsToPackages(packages, revisions)
sub addRevisionsToPackages{
    foreach my $pkgName(keys %{$_[0]}){
        if(exists $_[1]{$pkgName}){
            my $x = 0;
            while($x < $P2WDataOps::config->{'git_logs_to_show'} and $x < @{$_[1]{$pkgName}} ){
                push @{$_[0]->{$pkgName}->{'git_logs'}}, @{$_[1]{$pkgName}}[$x]->{'subject'};
                my @pv = $_[1]{$pkgName}[$x]->{'subject'} =~ /.* version ([0-9._-]+)$/;
                my $versionFound = $pv[0] || undef;
                if($x == 0 and defined $versionFound){
                    $_[0]->{$pkgName}->{'current_version'} = $versionFound;
                }
                if($_[0]->{$pkgName}->{'previous_version'} eq '' and $x > 0
                        and defined $versionFound and $versionFound ne $_[0]->{$pkgName}->{'current_version'}){
                    $_[0]->{$pkgName}->{'previous_version'} = $versionFound;
                }
                $x++;
            }
        }
    }
}

sub getRevList{
    my $limiter;
    if(defined $_[0]){
        $limiter = join(' ', @{$_[0]});
    } else {
        $limiter = '.';
    }
    my $dp = Git->repository(Directory => $config->{'input'});
    my @result = $dp->command('rev-list','--all','--timestamp','--since=\'Jan 01 2016\'','--format=%an%n%ai%n%s', '--', $limiter);
    return \@result;
}
sub getLastRevisions{
    my %result;
    my @gl = getRevList($_[0]);
    my $count = 0;
    while ($count < scalar @{$gl[0]}) {
        my $rev = revision_t();
        my @var = split(/ /, $gl[0][$count + 0]);
        $rev->{'timestamp'} = $var[0];
        $rev->{'hash'} = $var[2];
        $rev->{'author'} = $gl[0][$count + 1];
        $rev->{'date'} = $gl[0][$count + 2];
        $rev->{'subject'} = $gl[0][$count + 3];
        my @pkg = $gl[0][$count + 3] =~ /([^\s]+\/[^\s]+)/;
        $rev->{'package'} = $pkg[0];
        my @vp = $gl[0][$count + 3] =~ /.* version ([0-9_\.]+).*/;
        $rev->{'version_parsed'} = $vp[0];

        if(defined $rev->{'package'}){
            if(!defined $result{$rev->{'package'}}){
                $result{$rev->{'package'}} = ();
            }
            push @{$result{$rev->{'package'}}}, $rev;
        }
        $count += 4;
    }

    return \%result;
}

sub hideEmail{
    return $_[0] =~ s/\@.+$/$_[1]/r;
}

##
##
##
## older code that scanned every file on the filesystem directly
## instead of (ab)using git. Mostly a lot slower.
## Kept just in case git may not longer be used in said fashion.
##
## OLDER CODE STARTS HERE:
#sub getAvailablePorts(){
#
#    # holder for all packages
#    my @packages = ();
#
#    my $rule = File::Find::Rule->new(); # 1st stage
#    $rule->mindepth(2); # exclude the base dports directory
#    $rule->maxdepth(2);
#    $rule->directory;   # 2 stage enumeration, only look for directories in 1st stage
#    for my $pkgDir ($rule->in($P2WDataOps::config->{'input'})) {
#        my @ds = split(/\//, $pkgDir);
#        # non-port directories start uppercase
#        if( $ds[@ds - 2] =~ /^[a-z]/ ){#-e File::Spec->catfile($pkgDir, 'Makefile')){
#            push @packages, "$ds[@ds - 2]/$ds[@ds - 1]";
#        }
#    }
#
#    return \@packages;
#}
#
## loadPackageData(pkgName,noGit)
#sub loadPackageData{
#    my $doGit = 1;
#    if(exists $_[1] and $_[1] eq 'true'){
#        $doGit = 0;
#    }
#    my @var = split(/\//, $_[0]);
#    my $pkg = package_t();
#    $pkg->{'name'} = $var[1];
#    $pkg->{'category'} = $var[0];
#    $pkg->{'path'} = File::Spec->catdir($config->{'input'}, $_[0]);
#    my $makefile = File::Spec->catfile($pkg->{'path'}, 'Makefile');
#    my $pkgdescr = File::Spec->catfile($pkg->{'path'}, 'pkg-descr');
#    if ( -e $makefile ){
#        $pkg = parseMakefile(loadfile($makefile), $pkg);
#    }
#
#    if ( -e $pkgdescr){
#        $pkg = parsePkgDescr(loadfile($pkgdescr), $pkg);
#    }
#
#    if($doGit){
#        my $dp = Git->repository(Directory => $config->{'input'});
#        # determine the changes in this package from the git repository
#        foreach my $line ($dp->command('diff','--name-status','HEAD~1','HEAD', $pkg->{'path'})){
#            my ($status, $file) = split(/\s/, $line);
#            if($file eq File::Spec->catfile($_[0], 'Makefile')){
#                # file was just recently added
#                if($status eq 'A'){
#                    $pkg->{'status'} = 'New';
#                }
#                # modified or untracked
#                elsif($status eq 'M' or $status eq 'U'){
#                    $pkg->{'status'} = 'Modified';
#                    foreach my $diff ($dp->command('diff', 'HEAD~1', 'HEAD', '--', $pkg->{'path'})){
#                        if($diff =~ /^-PORTVERSION=\s.*/){
#                            my ($ign, $version) = split(/\s/, $diff);
#                            $pkg->{'previous_version'} = $version;
#                        }
#                    }
#
#                }
#                # deleted
#                elsif($status eq 'D'){
#                    $pkg->{'status'} = 'Deleted';
#                }
#
#            }
#        }
#
#        # get last N logs for the package
#        my $sep = 'SEPERATOR_DFLY';
#        my $logs = $dp->command('log', '-n', $config->{'git_logs_to_show'},"--format=Commit: %H%nAuthor: %an%nDate: %ai%n %n   %s%n$sep", '--', $pkg->{'path'});
#        my @output = grep(/Commit: /, split(/$sep/, $logs));
#        $pkg->{'git_logs'} = \@output;
#    }
#
#    return $pkg;
#
#}
#sub parseMakefile{
#    # split the contents into lines
#    my $content = $_[0];
#    my $pkg = $_[1];
#
#    my @lines = split /^/m, $content;
#    foreach my $line(@lines){
#        chomp $line;
#        my @var = split(/=/, $line);
#        if(scalar @var > 1){
#            if    ($var[0] eq 'MAINTAINER'){ $pkg->{'maintainer'} = trim($var[1]) || '';
#                    $pkg->{'hashed_maintainer'} = hideEmail($pkg->{'maintainer'}, '_AT_' . crc32($pkg->{'maintainer'}));
#                    $pkg->{'hidden_maintainer'} = hideEmail($pkg->{'maintainer'}, '@HIDDEN'); }
#            elsif ($var[0] eq 'LICENSE'){ $pkg->{'license'} = trim($var[1]); }
#            elsif ($var[0] eq 'COMMENT'){ $pkg->{'short_description'} = trim($var[1]); }
#            elsif ($var[0] eq 'CATEGORIES'){ @{$pkg->{'categories'}} = split(/ /, trim($var[1]));}
#            elsif ($var[0] eq 'USERS'){ @{$pkg->{'users'}} = split(/ /, trim($var[1])) ;}
#            elsif ($var[0] eq 'GROUPS'){ @{$pkg->{'groups'}} = split(/ /, trim($var[1]));}
#            elsif ($var[0] eq 'BINFILES'){ @{$pkg->{'bin_files'}} = split(/ /, trim($var[1]));}
#            elsif ($var[0] eq 'PORTVERSION'){ $pkg->{'version'} = trim($var[1]); }
#            elsif ($var[0] eq 'DISTVERSION'){ $pkg->{'distversion'} = trim($var[1]); }
#            elsif ($var[0] eq 'USES'){ @{$pkg->{'uses'}} = split(/ /, trim($var[1]));}
#            elsif ($var[0] eq 'BROKEN'){ push @{$pkg->{'broken'}}, trim($var[1]);}
#            elsif ($var[0] =~ /^BROKEN_/){ push @{$pkg->{'broken_arch'}}, ($var[0] =~ s/^BROKEN_//r, trim($var[1])); }
#            elsif ($var[0] =~ /_BROKEN$/){ push @{$pkg->{'broken_feature'}}, ($var[0] =~ s/_BROKEN$//r, trim($var[1])); }
#            else{ debugprint("UNHANDLED VAR: $var[0] value $var[1]\n", '1:UNHANDLED_VARIABLE:PARSE_DATA'); }
#
#        }
#    }
#
#    return $pkg;
#
#}
#
#
## parsePkgDescr(fileContents, package_t)
#sub parsePkgDescr{
#    my $content = $_[0];
#    my $package = $_[1];
#
#    # split the contents into lines
#    my @lines = split /^/m, $content;
#    my @desc = ();
#
#    # find which lines are description and which are URL
#    foreach my $line(@lines){
#        if ($line =~ /WWW: .*/){
#            my @url = split(/ /, $line); # extract URL
#            push @{$package->{'url'}}, $url[-1];
#        } else {
#            push @desc, $line;
#        }
#    }
#    # reassemble the description
#    $package->{'description'} = join('', @desc);
#    return $package;
#}
#
## PORTS RELATED FUNCTIONS
## parseAllPorts()
## traverses the input directory and gathers all the required data
#sub parseAllPorts(){
#    # by_* lists
#    my %bycategory;
#    my %byalphabet;
#    my %bymaintainer;
#    my %maintainerpkgs;
#
#    # holder for all packages
#    my %packages = ();
#
#    my $rule = File::Find::Rule->new(); # 1st stage
#    $rule->mindepth(2); # exclude the base dports directory
#    $rule->maxdepth(2);
#    $rule->directory;   # 2 stage enumeration, only look for directories in 1st stage
#
#    #my $filesrule = File::Find::Rule->new(); # 2nd stage
#    #$filesrule->name('pkg-descr','Makefile'); # only interested in these 2 files for 2nd stage
#    #$filesrule->maxdepth(1); # sometimes they have subdirs, don't descend.
#    for my $pkgDir ($rule->in($P2WDataOps::config->{'input'})) { # 1st stage
#        my @ds = split(/\//, $pkgDir);
#        my $pkgName = "$ds[@ds - 2]/$ds[@ds - 1]";
#        my $pkg = loadPackageData($pkgName, 'true');
#        my $shortentry = $pkgName;
#        # process and save markdown file
#        if($pkg->{'name'} ne ''){
#            # we just processed a package directory
#            $packages{$pkgName} = $pkg;
#            #makedir(File::Spec->catdir($config->{'output'}, $pkg->{'category'}));
#            #trysave(File::Spec->catfile($config->{'output'}, $pkg->{'category'} . '/' . $pkg->{'name'} . '.mdwn'),$page, $config);
#            # by category
#            if(!exists $bycategory{$pkg->{'category'}}){
#                $bycategory{$pkg->{'category'}} = ();
#            }
#            push @{$bycategory{$pkg->{'category'}}}, $shortentry;
#
#            # by alphabet
#            my $letter = lc(substr($pkg->{'name'},0,1));
#            if(!exists $byalphabet{$letter}){
#                $byalphabet{$letter} = ();
#            }
#            push(@{ $byalphabet{$letter} }, $shortentry);
#
#            # by maintainer
#            if($pkg->{'maintainer'} ne ''){
#                $letter = lc(substr($pkg->{'maintainer'},0,1));
#                if(!exists $bymaintainer{$letter}){
#                    $bymaintainer{$letter} = ();
#                    push @{$bymaintainer{$letter}}, 1;
#                } else {
#                    @{$bymaintainer{$letter}}[0]++;
#                }
#                # store the pkgs per maintainer
#                if(!exists $maintainerpkgs{$pkg->{'hashed_maintainer'}}){
#                    $maintainerpkgs{$pkg->{'hashed_maintainer'}} = ();
#                }
#                push @{$maintainerpkgs{$pkg->{'hashed_maintainer'}}}, $shortentry;
#            }
#        }
#        debugprint("seeking in $pkgDir\n", '5:DIR_ENTERED:FILE_OPS');
#    } # 1st stage end
#
#    #$Storable::Deparse = 1;
#    #store \%packages, 'packages_db';
#    #print Dumper(\%packages);
#    #debugprint("${keys %packages} packages parsed\n", '0:COUNT:INFO');
#    return (\%bycategory, \%byalphabet, \%bymaintainer, \%maintainerpkgs, \%packages);
#}

1;
